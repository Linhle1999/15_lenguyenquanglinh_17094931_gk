import React, { useState, useEffect } from "react";
import "./home.css";
import "antd/dist/antd.css";
import { Layout, Menu, Breadcrumb, Row, Col, Table, Modal, Input, Button } from "antd";
import { format } from "date-fns";

import axios from "./configaxios";

const { Header, Content } = Layout;
const { TextArea } = Input;

function Home() {
  const [data, setData] = useState([]);
  // const [visible, setVisible] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  // const [noiDung, setNoiDung] = useState("");
  // const [idcuads, setIDDs] = useState([]);

  useEffect(() => {
    axios
      .get("/get")
      .then((req) => {
        req.data.map((value) => {
          // setIDDs((data) => [...data, value.id]);

          setData((oldArray) => [
            ...oldArray,
            {
              key: value.maSP,
              maSP: value.maSP,
              tenSP: value.tenSP,
              soLuong: value.soLuong,
            },
          ]);
        });
      })
      .catch((err) => {
        console.log("Bug");
      });
  }, []);

  const columns = [
    {
      title: "Ma SP",
      dataIndex: "maSP",
      key: "maSP",
      width: 10,
    },
    {
      title: "Ten SP",
      dataIndex: "tenSP",
      key: "tenSP",
      width: 20,
    },
    {
      title: "So Luong",
      dataIndex: "soLuong",
      key: "soLuong",
      width: 40,
    },
    {
      title: "Chon",
      key: "operation",
      fixed: "right",
      width: 30,
      render: (text, row) => <Button type="primary" onClick={() => {
       const id=row.maSP;
        axios.delete(`/delete/${id}`).then((req) => {
          window.location.reload(false);
        }).catch((err) => {

        })
      }}>Xoa</Button>,
    },
  ];

  // function showModal() {
  //   setVisible(true);
  // }

  // function handleOk() {
  //   axios.post("/add", {
  //     ngaydang: format(new Date(), "yyyy-MM-dd"),
  //     noidung: noiDung,
  //     tacgia: tacGia,
  //     id: Math.max.apply(null, idcuads) + 1,
  //   });
  //   // setVisible(false);
  //   window.location.reload(false);

  // }

  // function handleCancel() {
  //   setVisible(false);
  // }

  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">Danh sách</Menu.Item>
          {/* <Menu.Item
            key="2"
            onClick={() => {
              showModal();
            }}
          >
            Thêm Mới
          </Menu.Item> */}
        </Menu>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        {/* <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb> */}
        <div className="site-layout-content">
          {/* <Row gutter={[16, 16]}>
            <Col span={6} >1</Col>
            <Col span={6} >2</Col>
            <Col span={6} >3</Col>
          </Row> */}

          <h5>Danh sach San Pham</h5>
          <Button type="primary" onClick={() => {
            console.log(selectedRowKeys);
          }}>Xoa</Button>
          <Table
            // rowSelection={rowSelection}
            columns={columns}
            dataSource={data}
          />
        </div>
      </Content>
      {/* 
      <Modal
        title="Basic Modal"
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Input
          placeholder="Tác giả"
          value={tacGia}
          onChange={(e) => {
            setTacGia(e.target.value);
          }}
        />
        <TextArea
          placeholder="Nội dung"
          style={{ marginTop: "20px", height: "300px" }}
          value={noiDung}
          onChange={(e) => {
            setNoiDung(e.target.value);
          }}
        />
      </Modal> */}
    </Layout>
  );
}

export default Home;
