import axios from 'axios';
const instance = axios.create({
    // baseURL: 'http://54.255.215.1:80/api/'
    baseURL: 'http://localhost:5000/'
});
export default instance;