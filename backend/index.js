const express = require("express");
const aws = require("aws-sdk");
const app = express();
const cors = require('cors');

app.use(express.json({ extended: false }));
app.use(cors()); // Use this after the variable declaration
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next(); 
});

const dynamoDB = new aws.DynamoDB.DocumentClient({
    
});

app.post("/add", (req, res) => {
    const { maSP, tenSP, soLuong } = req.body;
    let data = {
        maSP: maSP,
        tenSP: tenSP,
        soLuong: soLuong,
    };
    const params = {
        TableName: "15_lenguyenquanglinh_17094931_db",
        Item: data,
    };
    dynamoDB.put(params, (err, data) => {
        if (err) {
            console.log(err);
        } else {
            res.json({ msg: "Thêm thành công!!!" });
        }
    });
});

app.get("/get/:id", (req, res) => {
    const { maSP } = req.params;

    const params = {
        TableName: "15_lenguyenquanglinh_17094931_db",
        Key: {
            maSP: maSP,
        },
    };

    dynamoDB.get(params, (err, data) => {
        if (err) {
            console.error(
                "Unable to read item. Error JSON:",
                JSON.stringify(err, null, 2)
            );
        } else {
            //   console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            res.json(data.Items);
        }
    });
});

app.get("/get", (req, res) => {
    const params = {
        TableName: "15_lenguyenquanglinh_17094931_db",
    };

    dynamoDB.scan(params, (err, data) => {
        if (err) {
            console.error(
                "Unable to read item. Error JSON:",
                JSON.stringify(err, null, 2)
            );
        } else {
            //   console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            res.json(data.Items);
        }
    });
});

app.delete("/delete/:maSP", (req, res) => {
    const { maSP } = req.params;

    const params = {
        TableName: "15_lenguyenquanglinh_17094931_db",
        Key: {
            maSP: maSP,
        },
    };

    dynamoDB.delete(params, (err, data) => {
        if (err) {
            console.error(
                "Unable to read item. Error JSON:",
                JSON.stringify(err, null, 2)
            );
        } else {
            //   console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            res.json(data.Items);
        }
    });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log("Starting Backend"));
