import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from './Home';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path={['/', '/home']} exact component={Home} />
          {/* <Route exact component={NotFound} /> */}
        </Switch>
      </BrowserRouter>
    )
  }

}

export default App;